function createElement(tagName, className, text) {
    const el = document.createElement(tagName);
    el.className = className;
    const elText = document.createTextNode(text);
    el.appendChild(elText);

    return el;
}

document.addEventListener('DOMContentLoaded', () => {
    const form = document.querySelector('#myForm');

    form.addEventListener('submit', (event) => {
        event.preventDefault();
        const url = document.getElementById('source').value;

        const xhr = new XMLHttpRequest();
        xhr.open("POST", "/api/my");
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        xhr.onload = () => {
            if (xhr.status !== 200) {
                return;
            }
            const response = JSON.parse(xhr.response);
            if (response.content) {
                const container = document.querySelector('.container');
                response.content.forEach(item => {
                    const sector = createElement('div', 'item', item.sector);
                    const row = createElement('div', 'item', item.row);
                    const seat = createElement('div', 'item', item.seat);
                    const price = createElement('div', 'item', item.price);
                    container.appendChild(sector);
                    container.appendChild(row);
                    container.appendChild(seat);
                    container.appendChild(price);
                });
                container.style.display = 'grid';
            } else {
                const el = document.querySelector('.error')
                el.innerHTML = response.error;
                el.style.display = 'block';
            }
        };
        xhr.send(url);
    });
});