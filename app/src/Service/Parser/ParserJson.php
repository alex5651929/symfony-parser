<?php

namespace App\Service\Parser;


class ParserJson extends Parser
{
    public function parse(string $raw): array
    {
        $content = $this->getContent($raw, $this->config[0]);

        $result = json_decode($content, true);

        return $result["tickets"];
    }
}