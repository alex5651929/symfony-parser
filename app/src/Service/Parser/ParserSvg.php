<?php

namespace App\Service\Parser;


use Symfony\Component\DomCrawler\Crawler;

class ParserSvg extends Parser
{
    public function parse(string $raw): array
    {
        $content = $this->getContent($raw, $this->config[0]);
        $this->crawler->addXmlContent($content);
        $rows = [];
        $this->crawler->filterXPath($this->config[1])->each(function (Crawler $crawler) use (&$rows) {
            $rows[] = $crawler->text();
        });
        $this->crawler->clear();

        $result = [];
        foreach ($rows as $key => $item) {
            $tmp = explode('|', $item);
            foreach ($tmp as $part) {
                $param = explode(':', trim($part));
                $keyPart = strtolower(trim($param[0]));
                $value = trim($param[1]);

                switch ($keyPart) {
                    case 'price':
                        $result[$key][$keyPart] = (int)str_replace('$', '', $value);
                        break;
                    case 'row':
                    case 'seat':
                        $result[$key][$keyPart] = (int)$value;
                        break;
                    default:
                        $result[$key][$keyPart] = $value;
                        break;
                }
            }
        }

        return $result;
    }
}