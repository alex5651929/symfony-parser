<?php

namespace App\Service\Parser;

use Symfony\Component\DomCrawler\Crawler;

abstract class Parser
{
    protected $crawler;
    protected $config;

    public function __construct(array $config, Crawler $crawler)
    {
        $this->crawler = $crawler;
        $this->config = $config;
    }

    public function getConfig()
    {
        return $this->config;
    }

    protected function getContent(string $raw, string $xPath): string
    {
        $this->crawler->addHtmlContent($raw);
        $content = $this->crawler->filterXPath($this->config[0])->html();
        $this->crawler->clear();

        return $content;
    }

    abstract public function parse(string $content): array;
}