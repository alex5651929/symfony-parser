<?php

namespace App\Service\Parser;


class ParserManager
{
    private array|null $parsers = null;

    public function __construct(?array $parsers)
    {
        $this->parsers = $parsers;
    }

    public function getParsers(): ?array
    {
        if (!empty($this->parsers)) {
            return $this->parsers;
        }

        return null;
    }
}