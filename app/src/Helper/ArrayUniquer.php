<?php

namespace App\Helper;

class ArrayUniquer
{
    public static function getUniqueArray(array $arr): array
    {
        $uniqueArray = [];
        foreach ($arr as $item) {
            if (!in_array($item, $uniqueArray, true)) {
                $uniqueArray[] = $item;
            }
        }

        return $uniqueArray;
    }
}