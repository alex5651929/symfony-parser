<?php

namespace App\Controller;

use App\Entity\Ticket;
use App\Helper\ArrayUniquer;
use App\Repository\TicketRepository;
use App\Service\Parser\Parser;
use App\Service\Parser\ParserManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api', name: 'app_api')]
class ApiController extends AbstractController
{
    #[Route('/data', name: 'data')]
    public function index(TicketRepository $ticketRepository): JsonResponse
    {
        $tickets = $ticketRepository->findAll();

        return $this->json(json_decode(json_encode($tickets)));
    }
    #[Route('/my', name: 'my')]
    public function my(Request $request, ParserManager $manager, EntityManagerInterface $em): JsonResponse
    {
        if ($request->getMethod() === 'POST' && $request->isXmlHttpRequest()) {
            $url = $request->getContent();
            $content = file_get_contents($url);
            $parsers = $manager->getParsers();

            $result = [];
            /** @var Parser $parser */
            foreach ($parsers as $parser) {
                $result = array_merge($result, $parser->parse($content));
            }

            $result = ArrayUniquer::getUniqueArray($result);

            foreach ($result as $item) {
                $ticket = new Ticket();
                $ticket->setSector($item['sector']);
                $ticket->setRange($item['row']);
                $ticket->setSeat($item['seat']);
                $ticket->setPrice($item['price']);

                $em->persist($ticket);
                $em->flush();
            }

            return new JsonResponse(['content' => $result]);
        }

        return new JsonResponse(['error' => 'Oops, something went wrong.']);
    }
}
