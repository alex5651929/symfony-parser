## Инструкция

    1. git clone git@gitlab.com:alex5651929/symfony-parser.git
    2. в папке app выполнить команду: cp .env .env.local
    3. в файле .env.local прописать => DATABASE_URL="mysql://root:secret@database:3306/symfony_docker?serverVersion=8&charset=utf8mb4"
    4. make up
    5. make bash-app
    6. bin/console make:migration
    7. bin/console doctrine:migrations:migrate
    8. переходим на страницу: http://localhost:8080/
    9. копируем ссылку на страницу которую будем парсить в input поле => http://allkoncert.ru/test.html
    10. нажимаем кнопку 'Парсить'
    11. получаем результат: уникальный массив данных без повторений
    12. проходим на страницу: http://localhost:8080/api/data
    13. получаем результат: записи из базы данных
