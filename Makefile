up:
	docker-compose up -d

down:
	docker-compose down

bash-app:
	docker-compose exec php /bin/bash

bash-db:
	docker-compose exec database /bin/bash